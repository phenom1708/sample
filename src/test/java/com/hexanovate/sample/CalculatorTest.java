package com.hexanovate.sample;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    public void shouldReturnSameNumberIfAddedWithZero() {
        Calculator calculator = new Calculator();
        int expectedResult = 5;

        int actualResult = calculator.add(5, 0);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void shouldReturnSameNumberIfAddedWithOne() {
        Calculator calculator = new Calculator();
        int expectedResult = 5;

        int actualResult = calculator.add(5, 1);

        assertEquals(expectedResult, actualResult);
    }
}